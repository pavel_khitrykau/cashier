﻿namespace Cashier.Model
{
    public class ProductWithPricePerPound : Product
        {
            public float Pounds { get; set; }
        }
}
