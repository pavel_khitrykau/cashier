﻿using Cashier.Model.Interfaces;

namespace Cashier.Model
{
    public class Coupon : ICoupon
    {
        private double _discount;
        private double _priceToReceiveDiscount;
        public Coupon(double discout, double priceToReceiveDiscount)
        {
            _discount = discout;
            _priceToReceiveDiscount = priceToReceiveDiscount;
        }

        //returns discount if the purchase price is over required price for a discount
        public double GetDiscount(double price)
        {
            if (price >= _priceToReceiveDiscount)
            {
                return _discount;
            }
            else
            {
                return 0;
            }
        }
    }
}