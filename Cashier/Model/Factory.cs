﻿using Cashier.Model;
using Cashier.Model.Interfaces;

namespace Cashier.Model
{
    //Factory pattern is usufull for creating new calculators if required
    //This way all client needs to do it to pass type of calculator he wants to use
    public static class Factory
    {
        public static ICalculator getCalculator(string parameter)
        {
            switch (parameter)
            {
                case "buy3get1free":
                    return new PriceCalculatorDiscounted(3);
                default:
                    return new PriceCalculatorRegular();
            }
        }
    }
}