﻿namespace Cashier.Model.Interfaces
{
    public interface ICoupon
    {
        double GetDiscount(double price);
    }
}