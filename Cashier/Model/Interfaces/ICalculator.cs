﻿using System.Collections.Generic;

namespace Cashier.Model.Interfaces
{
    public interface ICalculator
    {
        double GetPrice(IEnumerable<Product> products);
    }
}