﻿using System.Collections.Generic;
using Cashier.Model.Interfaces;

namespace Cashier.Model
{
    public class PriceCalculatorDiscounted : ICalculator
    {
        private int _amountOfProductsToGetAFreeProduct;

        public PriceCalculatorDiscounted(int amountOfProductsToGetAFreeProduct)
        {
            _amountOfProductsToGetAFreeProduct = amountOfProductsToGetAFreeProduct;
        }

        public double GetPrice(IEnumerable<Product> products)
        {
            //string is for SKU, int for quantity
            //Dictionary is good for searching
            Dictionary<string, int> productDuplicates = new Dictionary<string, int>();
            double totalPrice = 0;
            //we use it to ckeck if free product was alredy given for the same SKU
            bool receivedFreeProduct = false;
            // Hashset of SKUs is good for finding duplicates and storing only unique SKU
            HashSet<string> addedProductsUnique = new HashSet<string>();
            //cashier can enter multiple items with the same SKU as many times as she wants
            int productsQuantity;
            foreach (var product in products)
            {
                productsQuantity = GetNumberOfItems(product);
                for (int i = 0; i < productsQuantity; i++)
                {
                    if (addedProductsUnique.Contains(product.SKU))
                    {
                        //if we already added the same product - increase quantity
                        if (productDuplicates.ContainsKey(product.SKU))
                        {
                            productDuplicates[product.SKU]++;
                        }
                        else
                        {
                            //if discount was applied already, start new count
                            if (receivedFreeProduct)
                            {
                                productDuplicates.Add(product.SKU, 1);
                            }
                            //first product went to hashset, current product goes to dictionary
                            //current product is second.
                            else
                            {
                                productDuplicates.Add(product.SKU, 2);
                            }
                        }
                    }
                    else if(product.GetType() == typeof(ProductWithPricePerItem))
                    {
                        addedProductsUnique.Add(product.SKU);
                    }

                    //if the amount of products in dictionary equls to required amount to get a free product
                    // we don't add a price(make it free) and delete it from dictionary in case we won't have same 
                    // products anymore - that is more likely
                    if (productDuplicates.ContainsKey(product.SKU)
                        && productDuplicates[product.SKU] == _amountOfProductsToGetAFreeProduct)
                    {
                        productDuplicates.Remove(product.SKU);
                        receivedFreeProduct = true;
                    }
                    else
                    {
                        //if the product is in pounds just calculate totalprice
                        //and it will go out of the loop itself
                        if (product.GetType() == typeof(ProductWithPricePerPound))
                        {
                            totalPrice += ((ProductWithPricePerPound)product).Pounds * product.PricePerUnit;
                        }
                        else
                        {
                            totalPrice += product.PricePerUnit;
                        }
                    }
                }
            }
            return totalPrice;
        }

        //Method returns the number of products if passed parameter is of type ProductWithPricePerItem
        //Otherwise it returns 1. We use it to calculate dicount based on quantity (buy 2 get 1 free)
        private static int GetNumberOfItems(Product product)
        {
            int productsQuantity;
            if (product.GetType() == typeof(ProductWithPricePerItem))
            {
                productsQuantity = ((ProductWithPricePerItem)product).Quantity;
            }
            else productsQuantity = 1;
            return productsQuantity;
        }
    }
}