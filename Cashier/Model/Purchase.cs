﻿using System.Collections.Generic;
using Cashier.Model.Interfaces;

namespace Cashier.Model
{
    //We want use single responsability principle (S in SOLID) and put
    //the Calculation logic into separete class. That is why we have PriceCalculator class.
    //We also want to use dependecny injection here(D in SOLID), so when we are working with unit tests
    //there is no dependency on concrete class. If we don't use interface here - any changes 
    // in PriceCalculator Class logic can break the tests, written against Purchase.
    // So we just want to use interfces ICalculator and ICoupon
    public class Purchase
    {
        private ICalculator calculator;
        private ICoupon coupon;
        private double totalPrice;
        private double discount;
        public IEnumerable<Product> Products { get; set; }

        //cuouponToUse is optional
        public Purchase(ICalculator calculatorToUse, ICoupon couponToUse = null)
        {
            calculator = calculatorToUse;
            coupon = couponToUse;
        }

        public double GetFullPrice()
        {
            totalPrice = calculator.GetPrice(this.Products);
            return totalPrice;
        }

        public double GetDiscout()
        {
            discount = coupon.GetDiscount(totalPrice);
            return discount;
        }
    }
}