﻿namespace Cashier.Model
{
    public class ProductWithPricePerItem : Product
    {
        public int Quantity { get; set; }
    }
}