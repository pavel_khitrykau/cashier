﻿using System.Collections.Generic;
using Cashier.Model.Interfaces;

namespace Cashier.Model
{
    public class PriceCalculatorRegular : ICalculator
    {
        public double GetPrice(IEnumerable<Product> products)
        {
            double totalPrice = 0;
            foreach (var product in products)
            {
                if (product.GetType() == typeof (ProductWithPricePerItem))
                {
                    totalPrice += product.PricePerUnit * ((ProductWithPricePerItem)product).Quantity;
                }
                else
                {
                    totalPrice += product.PricePerUnit * ((ProductWithPricePerPound)product).Pounds;
                }
            }
            return totalPrice;
        }
    }
}