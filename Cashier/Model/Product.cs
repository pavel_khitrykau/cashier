﻿namespace Cashier.Model
{
    //This class is just an abstraction, we want to prohibit the client
    //to create an isntance of this class beacase this way 
    //we will miss necessary properties like Pounds or Quantity
    public abstract class Product
    {
        public string SKU { get; set; }
        public float PricePerUnit { get; set; }//Price per pound or per quantity
    }
}
