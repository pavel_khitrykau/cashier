﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Cashier;
using Cashier.Model;

namespace UnitTests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void ResultForRegularCalculator()
        {
            var calculator = new PriceCalculatorRegular();
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 4.5f},
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 10 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 2},
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 175);
        }
        [TestMethod]
        public void ResultForPriceCalculatorBuyFourGetOneFree_FiveSameProducts()
        {
            var calculator = new PriceCalculatorDiscounted(4);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 1},
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10,Quantity = 1 },
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 50);
        }

        [TestMethod]
        public void ResultForPriceCalculatorBuyFourGetOneFree_TenSameProducts()
        {
            var calculator = new PriceCalculatorDiscounted(4);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 1},
                    //2 products with the same SKU should be free
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 90);
        }

        [TestMethod]
        public void ResultForPriceCalculatorBuyFourGetOneFree_TwoDifferentProductsFiveEach()
        {
            var calculator = new PriceCalculatorDiscounted(4);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 1},
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 90);
        }

        [TestMethod]
        public void ResultForPriceCalculatorBuyFourGetOneFree_TwoDifferentProductsFiveEachInDifferentOrder()
        {
            var calculator = new PriceCalculatorDiscounted(4);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 1},
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 1 },
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 90);
        }

        [TestMethod]
        public void ResultForCalculatorBuyNineGetOneFreeWithDifferentQuantity()
        {
            var calculator = new PriceCalculatorDiscounted(9);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 10 },
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 6 },
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 180);
        }

        [TestMethod]
        public void ResultForCalculatorBuyNineGetOneFreeWithDifferentQuantityAndDifferentOrder()
        {
            var calculator = new PriceCalculatorDiscounted(9);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 5 },
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 5 },
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerItem(){SKU = "a1002", PricePerUnit = 10, Quantity = 10 },//140
                    new ProductWithPricePerItem(){SKU = "a1003", PricePerUnit = 10, Quantity = 5 },//90
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 290);

        }

        [TestMethod]
        public void ResultForCoupon()
        {
            // first argument is discount in dollars, second argument is minimum purchase amount to receive a discount
            var coupon = new Coupon(5.5,100);
            double price = 120;
            var reducedPrice = price - coupon.GetDiscount(price);
            Assert.AreEqual(reducedPrice, 114.5);
        }

        [TestMethod]
        public void ResultForDicountedCalculatorWithFiveItemsPerPoundBuyFourGetOneFree()
        {
            var calculator = new PriceCalculatorDiscounted(4);
            var purchase = new Purchase(calculator)
            {
                Products = new List<Product>(){
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                    new ProductWithPricePerPound(){SKU = "a1001", PricePerUnit = 10, Pounds = 3},//30
                }
            };
            var totalPrice = purchase.GetFullPrice();
            Assert.AreEqual(totalPrice, 150);
        }
      

    }
}
